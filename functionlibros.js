/*
Se llama a la const con el objeto autores(mi array)
y se hace un require del archivo donde se encuentra
*/
const { autores } = require("./functionautores");

/*
Se crea la funcion que me de los libros que posee un autor.
Inicia con una  constante donde se llame al id que me estan 
dando de parametro en el path de mi endpoint y a su vez la 
transformo en numero, ya que sino la toma como string.
Luego uso un If para confirmar que ambos id coinciden y si es
asi, se hace un send de mi autor elegido (NO DE TODO EL ARRAY
SINO SOLAMENTE DE UN AUTOR) y ahí si del array Libros
 */
function librosAutor(req, res) {
    const idDado = Number(req.params.id);
    for (const autor of autores) {
        if (idDado === autor.id) {
            res.send(autor.libros);
        }
    }
}

/*
Funcion para crear un nuevo libro, nuevamente deben coincidir
los id. Luego con un for recorro array y con el if constato
que si ambos id coinciden debo hacer un push a mi autor, a su
array de libros, donde la informacion venga del body.
El console log es para confirmar que se encuentra cargado y
luego un send donde me confirme la operacion completa.
*/
function nuevoLibro(req, res) {
    const idDado = Number(req.params.id);
    for (const autor of autores) {
        if (idDado === autor.id) {
            autor.libros.push(req.body);
            console.log(autor.libros);
            res.status(202).send('libro cargado');
        }
    }
}

/*
Se crea la funcion que nos va a devolver 1 solo libro.
Se comparan que coincidan los Id y luego que coincidan
los Id del libro. Se utiliza un for para recorrer el array
y un if que compare los Id, dentro del IF utilizo un for
para recorrer EL ARRAY DE LIBROS y ahí utilizo un if
que compare los id de los libros. Luego se envio la info
del libro seleccionado
*/
function devolverLibro(req, res) {
    const idDado = Number(req.params.id);
    const idDadoLibro = Number(req.params.idLibro);
    for (const autor of autores) {
        if (idDado === autor.id) {
            for (const libro of autor.libros) {
                if (idDadoLibro === libro.id) {
                    res.send(libro);
                }
            }
        }
    }
}

/*
Se crea la funcion que nos va a dejar modificar un libro.
Se comparan que coincidan los Id y luego que coincidan
los Id del libro. Se utiliza un for para recorrer el array
y un if que compare los Id, dentro del IF utilizo un for
para recorrer EL ARRAY DE LIBROS y ahí utilizo un if
que compare los id de los libros. Luego utiliza la comparacion
del titulo de mi libro, donde se envie la informacion del body.
*/
function modificarLibro(req, res) {
    const idDado = Number(req.params.id);
    const idDadoLibro = Number(req.params.idLibro);
    for (const autor of autores) {
        if (idDado === autor.id) {
            for (const libro of autor.libros) {
                if (idDadoLibro === libro.id) {
                    libro.titulo = req.body.titulo;
                    res.send('libro modificado');
                }
            }
        }
    }
}

/*
Se crea la funcion que nos va a dejar eliminar un libro.
Se comparan que coincidan los Id y luego que coincidan
los Id del libro. Se utiliza un for para recorrer el array
y un if que compare los Id, dentro del IF utilizo un for
para recorrer EL ARRAY DE LIBROS y ahí utilizo un if
que compare los id de los libros. Luego creo la const
que me va a dar, con el autor en especial, en su array,
el index de 1 solo libro. y luego el splice, del index
de ese libro mencionado.
*/
function eliminarLibro(req, res) {
    const idDado = Number(req.params.id);
    const idDadoLibro = Number(req.params.idLibro);
    for (const autor of autores) {
        if (idDado === autor.id) {
            for (const libro of autor.libros) {
                if (idDadoLibro === libro.id) {
                    const posicionLibro = autor.libros.indexOf(libro);
                    autor.libros.splice(posicionLibro, 1);
                    res.send('libro eliminado');
                }
            }
        }
    }
}

/*
La funcion MIDDLEWARE que te permite confirmar que 
un autor exista y a su vez que me deje confirmar
que el id de libros tambien existe.
Si ambos parametros estan te permite que continue 
con las demás funciones. En caso de que no exista, 
deja de ejecutar
*/
function libroExiste(req, res, next) {
    const idDado = Number(req.params.id);
    const idDadoLibro = Number(req.params.idLibro);
    for (const autor of autores) {
        if (idDado === autor.id) {
            for (const libro of autor.libros) {
                if (idDadoLibro === libro.id) {
                    next();
                    return;
                }
            }
            res.send('libro no encontrado');
        }
    }
}

/*
Para poder llamar a la funciones desde el /.app
se debe hacer un module.exports que contenga
los objetos de las funciones
*/
module.exports = {
    librosAutor,
    nuevoLibro,
    devolverLibro,
    modificarLibro,
    eliminarLibro,
    libroExiste
}