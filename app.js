//llamas a la libreria express
const express = require('express');

//le das un nombre a tu libreria bajo una constante
const server = express();

//bajo una constante que tenga como objeto a tus funciones, las llamas//
const { iniciarServidor, 
        devolverAutores,
        crearAutor, 
        autoresPorId, 
        modificarAutorPorId,
        eliminarAutorPorId, 
        escritorExiste } = require('./functionautores'); 

const { librosAutor, 
        nuevoLibro, 
        devolverLibro,
        modificarLibro, 
        eliminarLibro, 
        libroExiste } = require('./functionlibros');


//llamas a la funcion middleware global de json, la cual se usa para sacar los datos del body//
server.use(express.json());

// un get para devolver los autores
server.get('/autores', devolverAutores); 

// un post para crear nuevos autores
server.post('/autores', crearAutor); 
// un middleware para los siguientes endpoints que no continue la funcion si el autor no existe
server.use('/autores/:id', escritorExiste); 
// un get para identificar a cada autor por su ID
server.get('/autores/:id', autoresPorId); 
// un delete para eliminar autores por su ID
server.delete('/autores/:id', eliminarAutorPorId); 
// un put para modificar ciertos datos de un autor por su ID
server.put('/autores/:id', modificarAutorPorId); 

//un get para devolver los libros por autor
server.get('/autores/:id/libros', librosAutor);
//un post para poder crearle un nuevo libro a un autor
server.post('/autores/:id/libros', nuevoLibro);
//un middlewate para los siguientes endpoint que no continue si el numero de ID del libro no existe, se complementa con el middleware anterior
server.use('/autores/:id/libros/:idLibro', libroExiste);
//un get para indentificar a cada libro por su ID y su autor
server.get('/autores/:id/libros/:idLibro', devolverLibro);
//un put para modificar ciertos datos de un libro por su ID y su autor
server.put('/autores/:id/libros/:idLibro', modificarLibro);
//un delete para eliminar libros
server.delete('/autores/:id/libros/:idLibro', eliminarLibro);

//el puerto que voy a utilizar
server.listen(9600, iniciarServidor);