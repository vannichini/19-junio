/*
Es la primer funcion que se crea
la cual nos va a mostrar si el servidor está activo y funcionando
*/
function iniciarServidor() {
    console.log('Servidor Iniciado');
}

/*
Creas el array de autores, que a su vez contiene objetos
que poseen dentro de ellos otro array de libros con sus objetos.
es una mamushka de array, objeto, array, objeto.
*/
let autores =
    [{
        id: 1,
        nombre: "Jorge Luis",
        apellido: "Borges",
        bornDate: "24/08/1889",
        libros: [{
            id: 1,
            titulo: "Ficciones",
            descripcion: "De terror es este",
            publicacion: 1944
        },
        {
            id: 2,
            titulo: "El Aleph",
            descripcion: "Este es de cuando pegaba la locura",
            publicacion: 1949
        }
        ]
    },
    {
        id: 2,
        nombre: "Gabriel",
        apellido: "Garcia Marquez",
        bornDate: "06/03/1927",
        libros: [{
            id: 1,
            titulo: "100 anios de soledad",
            descripcion: "De fantasia, un lujo.",
            publicacion: 1967
        },
        {
            id: 2,
            titulo: "El coronel no tiene quien le escriba",
            descripcion: "Este es de soledad a pleno..",
            publicacion: 1961
        },
        {
            id: 3,
            titulo: "La horasca",
            descripcion: "De la weed es este..",
            publicacion: 1955
        }]
    },
    {
        id: 3,
        nombre: "Juan",
        apellido: "Gonzalez",
        bornDate: "30/05/1994",
        libros: [{
            id: 1,
            titulo: "La prosa de la locura..",
            descripcion: "Estaba en una nota eterna..",
            publicacion: 2014
        },
        {
            id: 2,
            titulo: "Crazy and young",
            descripcion: "Un kamikaze",
            publicacion: 2016
        }]
    }];


/*
Creo la funcion para devolver los nombres y apellidos 
de todos los autores dentro de mi array para eso uso 
una funcion con los parametos (req,res) donde creo un 
nuevo array donde se van a guardar los datos que necesito 
que me devuelva el endpoint. Con el for recorro mi array 
ya existente (autores) donde voy a tomar un objeto solo, 
y hacer un push a mi nuevo array, del dato nombre y apellido DE ESE 
OBJETO SOLAMENTE. Luego envio un send con ese array nuevo creado.
*/
function devolverAutores(req, res) {
    let nombreAutores = [];
    for (const autor of autores) {
        nombreAutores.push(autor.nombre + ' ' + autor.apellido);
    }
    res.send(nombreAutores);
}

/*
Creo la funcion para crear un nuevo autor, donde uso el array 
autores y le hago un push que llame al body, de esta forma los 
datos que yo cargue en el body, se van a sumar a mi array
y luego hago un send solamente para confirmar que paso 
por toda la funcion y me diga que esta ok
*/
function crearAutor(req, res) {
    autores.push(req.body);
    res.status(202).send('nuevo autor cargado');
}

/*
La funcionar para buscar un autor por su id, inicia con una 
constante donde se llame al id que me estan dando de parametro 
en el path de mi endpoint y a su vez la transformo en numero, 
ya que sino la toma como string. Luego uso el for para que recorra 
mi array y se usa el if para que en caso de que coincida el id 
dado con el id que ya tengo, me envie el nombre y apellido del autor.
*/
function autoresPorId(req, res) {
    const idDado = Number(req.params.id);
    for (const autor of autores) {
        if (idDado === autor.id) {
            res.send(autor.nombre + ' ' + autor.apellido);
            return;
        }
    }
}

/*
La funcion de elimiar autores por id tambien usa la constante de ID.
luego uso el for donde recorro mi array y creo una nueva constante 
que sea igual a mi array y indexOF(lo cual me permite saber la posicion 
dentro de mi array de un autor). Ahi utilizo un if donde si coinciden 
los id dados, se elimine de mi array al autor elegido segun su posicion 
*/
function eliminarAutorPorId(req, res) {
    const idDado = Number(req.params.id);
    for (const autor of autores) {
        const posicionAutor = autores.indexOf(autor);
        if (idDado === autor.id) {
            autores.splice(posicionAutor, 1);
            res.send('autor eliminado');
        }
    }
}

/*
La funcion que te permita modificar autores por su ID. 
Se usa nuevamente el compara ID y el for para recorrer 
nuestro array. Luego el if donde si ambos ID coinciden,
te permita pedir el nombre del autor y modificarlo segun
el parametro del body pero solo el nombre.
*/
function modificarAutorPorId(req, res) {
    const idDado = Number(req.params.id);
    for (const autor of autores) {
        if (idDado === autor.id) {
            autor.nombre = req.body.nombre;
            res.send('autor modificado');
        }
    }
}

/*
La funcion MIDDLEWARE que te permite confirmar que 
un autor exista y continue con las demás funciones.
En caso de que no exista, deja de ejecutar
*/
function escritorExiste(req, res, next) {
    const idDado = Number(req.params.id);
    for (const autor of autores) {
        if (idDado === autor.id) {
            next();
            return;
        }
    }
    res.send('autor no encontrado');
}

/*
Para poder llamar a la funciones desde el /.app
se debe hacer un module.exports que contenga
los objetos de las funciones
*/
module.exports = {
    iniciarServidor,
    autores,
    devolverAutores,
    crearAutor,
    autoresPorId,
    eliminarAutorPorId,
    modificarAutorPorId,
    escritorExiste
}